<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mails', function (Blueprint $table) {
	        $table->string('id')->unique();
	        $table->integer('user_id')->nullable();
	        $table->string('internalDate')->nullable();
	        $table->text('labels')->nullable();
	        $table->text('headers')->nullable();
	        $table->string('subject')->nullable();
	        $table->text('from')->nullable();
	        $table->string('fromName')->nullable();
	        $table->string('fromEmail')->nullable();
	        $table->text('to')->nullable();
	        $table->string('deliveredTo')->nullable();
	        $table->text('plainTextBody')->nullable();
	        $table->text('rawPlainTextBody')->nullable();
	        $table->boolean('attachments')->nullable();
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mails');
    }
}
