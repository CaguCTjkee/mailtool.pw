<tr data-id="{{ $mail->getId() }}">
    {!! dd($mail, $mail->getId()) !!}
    <td>{{ $mail->getId() }} {!! $mail->id !!} {{ $mail->getFromEmail() }}</td>
    <td>
        <a href="{{ route('mail', $mail->getId()) }}">{{ $mail->getSubject() }}</a>
    </td>
    <td>{!! App\Helpers\Helper::viewLabels($mail->getLabels()) !!}</td>
    <td>{{ $mail->getInternalDate() }}</td>
</tr>
