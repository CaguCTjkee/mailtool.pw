@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @if (session('status'))
                <div class="col-12">
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                </div>
            @endif
            <div class="col-md-3">
                @include('mail.parts.menu')
            </div>
            <div class="col-md-9">
                <table class="table">
                    <tr>
                        <th>Sender</th>
                        <th>Subject</th>
                        <th>Label</th>
                        <th>Date</th>
                    </tr>
                    @if( count($mails) )
                        @foreach($mails as $mail)
                            @include('mail.parts.mailTr')
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4">You dont have a new mails by subject <b>{!! request() !!}</b></td>
                        </tr>
                    @endif
                </table>
            </div>
        </div>
@endsection
