@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Login') }}</div>
                    <div class="card-body">
                        @if(LaravelGmail::check())
                        <h1>{{ LaravelGmail::user() }}</h1>
                            <a class="btn btn-primary" href="{{ url('oauth/gmail/logout') }}">Logout</a>
                        @else
                            <a class="btn btn-danger" href="{{ url('oauth/gmail') }}">Login by Google</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
