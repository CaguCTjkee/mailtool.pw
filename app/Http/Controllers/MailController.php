<?php

namespace App\Http\Controllers;

use App\Helpers\Gmail;
use Illuminate\Http\Request;
use LaravelGmail;

class MailController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('GmailAuth');
	}

	/**
	 * @param string $subject
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index($subject = 'Request trial')
	{
		$mails = LaravelGmail::message()->subject($subject)->unread()->preload()->all();

		if( count($mails) )
		{
			$mails = Gmail::mailsParse($mails);
		}

		return view('mail.home', compact('mails'));
	}

	/**
	 * @param string $mailId
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function mail(string $mailId)
	{
		$mail = Gmail::getMail($mailId);

		dd($mail);

		return view('mail.mail', compact('mail'));
	}
}
