<?php

namespace App\Http\Middleware;

use Closure;
use LaravelGmail;

class GmailAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	if( LaravelGmail::check() === false ) {
		    return redirect('login');
	    }

        return $next($request);
    }
}
