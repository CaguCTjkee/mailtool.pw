<?php
/**
 * Created by IntelliJ IDEA.
 * User: caguct
 * Date: 8/13/18
 * Time: 6:03 PM
 */

namespace App\Helpers;

use App\User;
use Illuminate\Support\Collection;
use LaravelGmail\Services\Message\Mail;
use LaravelGmail;

class Gmail {

	static function getMail($mailId)
	{
		$mail = [];
		$m = LaravelGmail::message()->get($mailId);

		return $mail;
	}

	/**
	 * @param Collection $mails
	 *
	 * @return array
	 */
	static function mailsParse(Collection $mails)
	{
		$result = [];

		if( count($mails) > 0 )
		{
			/**
			 * @var $mail \Dacastro4\LaravelGmail\Services\Message\Mail
			 */
			foreach( $mails as $mail )
			{
				$mailId    = $mail->getId();
				$mailModel = \App\Mail::where('id', $mailId)->first();
				if( $mailModel === null )
				{
					$mail->load();

					$user   = User::getAuthUser();
					$userId = $user->id;

					$mailModel                   = new \App\Mail();
					$mailModel->id               = $mailId;
					$mailModel->user_id          = $userId;
					$mailModel->internalDate     = $mail->getInternalDate();
					$mailModel->labels           = json_encode($mail->getLabels());
					$mailModel->headers          = json_encode($mail->getHeaders());
					$mailModel->subject          = $mail->getSubject();
					$mailModel->from             = json_encode($mail->getFrom());
					$mailModel->fromName         = $mail->getFromName();
					$mailModel->fromEmail        = $mail->getFromEmail();
					$mailModel->to               = json_encode($mail->getTo());
					$mailModel->deliveredTo      = $mail->getDeliveredTo();
					$mailModel->plainTextBody    = $mail->getPlainTextBody();
					$mailModel->rawPlainTextBody = $mail->getRawPlainTextBody();
					$mailModel->attachments      = $mail->hasAttachments() ? 1 : 0;
					$mailModel->save();
				}

				$result[] = $mailModel;
			}
		}

		return $result;
	}
}
