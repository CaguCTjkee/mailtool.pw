<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use LaravelGmail;

/**
 * Class User
 *
 * @property int    $id
 * @property string $name
 * @property string $email
 * @property string $password
 *
 * @property string $remember_token
 * @property string $created_at
 * @property string $modified_at
 *
 * @package App
 */
class User extends Authenticatable {

	use Notifiable;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'email',
		'password',
	];
	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token',
	];

	static function getAuthUser()
	{
		if( LaravelGmail::check() )
		{
			return self::where('email', LaravelGmail::user())->first();
		}
		else
		{
			return null;
		}
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function mails()
	{
		return $this->hasMany('App\Mail');
	}
}
