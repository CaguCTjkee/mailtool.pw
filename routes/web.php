<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');


Route::get('/subject/{subject}', 'MailController@index')->name('subject');
Route::get('/mail/{mailId}', 'MailController@mail')->name('mail');

Route::get('/oauth/gmail', function() {
	return LaravelGmail::redirect();
});

Route::get('/oauth/gmail/callback', function() {
	LaravelGmail::makeToken();

	$mail = LaravelGmail::user();

	$user = \App\User::where('email', $mail)->first();
	if( $user === null )
	{
		$user = new \App\User();
		$user->name = $mail;
		$user->email = $mail;
		$user->password = bcrypt(time());
		$user->save();
	}

	return redirect()->to('/');
});

Route::get('/oauth/gmail/logout', function() {
	LaravelGmail::logout(); //It returns exception if fails

	return redirect()->to('/');
});

Route::get('/profile', function() {
	return view('auth.profile');
})->name('profile');
